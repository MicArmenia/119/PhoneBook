﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.PhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Contact> contactList = CreateContacts(20);

            PhoneBook phoneBook = new PhoneBook();
            phoneBook.AddRange(contactList);
            Contact contact = phoneBook[2];

            Contact c = new Contact { Phone = "+374 TestPhoneNumber" };
            phoneBook[2] = c;

            phoneBook[c.Phone] = c;
            List<Contact> items = phoneBook.Find("93");

            Console.ReadLine();
        }

        private static List<Contact> CreateContacts(int count)
        {
            string[] codes = { "93", "98", "91", "77", "41", "55" };
            var rnd = new Random();

            List<Contact> contacts = new List<Contact>(count);
            for (int i = 0; i < count; i++)
            {
                int index = rnd.Next(0, codes.Length);

                var contact = new Contact();
                contact.Phone = $"+374 {codes[index]} {i} {i} {i}";
                contact.Name = $"A{i}";
                contact.Surname = $"A{i}yan";
                contact.Email = $"a{i}@gmail.com";

                contacts.Add(contact);
            }

            return contacts;
        }
    }
}
