﻿using System;

namespace Mic.Lesson.PhoneBook
{
    class Contact
    {
        private string phone;
        public string Phone
        {
            get => phone;
            set
            {
                if (value.StartsWith("+374") || value.StartsWith("00374"))
                    phone = value;
                else
                    Console.WriteLine("Incorrect format of number");
            }
        }

        public string Name { get; set; }
        public string Surname { get; set; }

        private string email;
        public string Email
        {
            get => email;
            set
            {
                if (value.EndsWith("@gmail.com") || value.EndsWith("@mail.ru") || value.EndsWith("@yahoo.com"))
                    email = value;
                else
                    Console.WriteLine("Incorrect format of Email");
            }
        }
        public string FullInformation => $"{phone}  {Name}  {Surname} {email}";

        public override string ToString()
        {
            return FullInformation;
        }
    }
}