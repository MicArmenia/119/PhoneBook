﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.HashSet
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText("Test.txt");

            HashSet<Char> set = new HashSet<char>(text);

            Random rnd = new Random();
            HashSet<int> set1 = new HashSet<int>();
            for (int i = 0; i < 100; i++)
            {
                int index = rnd.Next(0, 10);
                if (index == 5 || index == 3 || index == 1)
                    index = 6;

                set1.Add(index);
            }

            Console.ReadLine();
        }
    }
}
