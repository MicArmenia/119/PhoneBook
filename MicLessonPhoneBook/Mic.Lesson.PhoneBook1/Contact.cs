﻿using System;

namespace Mic.Lesson.PhoneBook1
{
    class Contact
    {
        public Contact(string phone)
        {
            if (phone.StartsWith("+374") || phone.StartsWith("00374"))
                this.Phone = phone;
            else
                throw new Exception("+374");
        }

        public readonly string Phone;

        public string Name { get; set; }
        public string Surname { get; set; }

        private string email;
        public string Email
        {
            get => email;
            set
            {
                if (value.EndsWith("@gmail.com") || value.EndsWith("@mail.ru") || value.EndsWith("@yahoo.com"))
                    email = value;
                else
                    Console.WriteLine("Incorrect format of Email");
            }
        }
        public string FullInformation => $"{Phone}  {Name}  {Surname} {email}";

        public override string ToString()
        {
            return FullInformation;
        }
    }
}