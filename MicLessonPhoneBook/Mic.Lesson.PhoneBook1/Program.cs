﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.PhoneBook1
{
    class Program
    {
        static void Main(string[] args)
         {
            List<Contact> contactList = CreateContacts(20);

            PhoneBook phoneBook = new PhoneBook();
            phoneBook.AddRange(contactList);

            var aaa = phoneBook["+374 41 6 6 6"];

            Contact c = new Contact("+374 TestPhoneNumber");
            phoneBook[2] = c;
            //phoneBook[2].Phone = "+374 TestPhoneNumber";

            Contact c1 = phoneBook["+374 TestPhoneNumber"];


            //phoneBook.AddRange(contactList);
            //Contact contact = phoneBook[2];

            //Contact c = new Contact { Phone = "+374 TestPhoneNumber" };
            //phoneBook[2] = c;

            //phoneBook[c.Phone] = c;
            //List<Contact> items = phoneBook.Find("93");

            Console.ReadLine();
        }

        private static List<Contact> CreateContacts(int count)
        {
            string[] codes = { "93", "98", "91", "77", "41", "55" };
            var rnd = new Random();

            List<Contact> contacts = new List<Contact>(count);
            for (int i = 0; i < count; i++)
            {
                int index = rnd.Next(0, codes.Length);

                string phone = $"+374 {codes[index]} {i} {i} {i}";
                var contact = new Contact(phone);
                contact.Name = $"A{i}";
                contact.Surname = $"A{i}yan";
                contact.Email = $"a{i}@gmail.com";

                contacts.Add(contact);
            }

            return contacts;
        }
    }
}
